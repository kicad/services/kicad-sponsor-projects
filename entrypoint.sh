#!/bin/bash
set +e

echo "Starting container as user: $(whoami)"

if [[ "${DEBUG}" == "true" ]]; then
    echo "Running in debug mode..."
    cp /etc/nginx/nginx_debug.conf /etc/nginx/conf.d/default.conf
else
    echo "Running in production mode..."
    cp /etc/nginx/nginx_prod.conf /etc/nginx/conf.d/default.conf
fi

if [[ "${UWSGI_PROFILE}" == "development" ]]; then
    echo "Running in development mode..."
    uwsgi --ini /etc/uwsgi.ini:development --logto /var/log/uwsgi.log
else
    echo "Running in production mode..."
    service nginx start && uwsgi --ini /etc/uwsgi.ini:production --logto /var/log/uwsgi.log
fi

# If uWSGI failed to start, print an error message and start a shell
if [[ "$?" -ne 0 ]]; then
    if [[ -e /var/log/uwsgi.log ]]; then
        echo "========================================"
        echo "UWSGI LOG:"
        cat /var/log/uwsgi.log
    fi
    echo "========================================"
    echo "NGINX LOG:"
    cat /var/log/nginx/error.log
    echo "Error: Failed to start docker services. Starting a shell instead."
    exec /bin/bash
fi
