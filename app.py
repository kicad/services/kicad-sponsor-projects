from flask import Flask, request, redirect, jsonify, render_template
from werkzeug.exceptions import NotFound
import logging
import os
import redis
import sys

app = Flask(__name__)

if os.getenv('DEBUG').lower() == 'true':
    app.debug = True
    logging.basicConfig(level=logging.DEBUG, stream=sys.stderr)
else:
    app.debug = False
    logging.basicConfig(level=logging.WARNING, stream=sys.stderr)

HOST = os.getenv('REDIS_HOST', 'localhost')
WEBHOOK_ID = os.getenv("WEBHOOK_ID")

def verify_request(request):
    webhook_id = request.headers['HTTP_PAYPAL_WEBHOOK_ID']
    if webhook_id is not WEBHOOK_ID:
        return "Invalid signature."

    return None

@app.route('/')
def index():
    return redirect('https://www.kicad.org')

r = redis.Redis(host=HOST, port=6379, db=0, decode_responses=True)

@app.route('/get_total_donations', methods=['GET'])
def get_total_donations():
    logging.debug(f"Received request: {request}")
    option = request.args.get('option')
    if not option:
        return jsonify({"error": "Option parameter is required"}), 400
    key = f'total:{option}'
    logging.debug(f"Received request for option '{option}'")

    try:
        total = r.get(key)
        if total is None:
            logging.debug(f"No donations found for option '{option}'")
            return jsonify({"error": f"No donations found for option '{option}'"}), 404
        else:
            logging.debug(f"Total donations for option '{option}': {total}")
            return jsonify({"option": option, "total_donations": total})
    except redis.exceptions.RedisError as e:
        logging.error(f"Redis error: {str(e)}")
        return jsonify({"error": f"Redis error: {str(e)}"}), 500
    
# import the conversion rates
from config import CONVERSION_RATES

@app.route('/donations', methods=['POST'])
def add_donation():
    logging.debug(f"Received request: {request}")
    error = verify_request(request)
    if error is not None:
        return jsonify({"error": error}), 400

    try:
        logging.debug(f"Received request: {request}")
        data = request.get_json()
        item_name = data.get('resource', {}).get('option_name')  # Update this as needed
        amount = data.get('resource', {}).get('amount', {}).get('total')
        currency = data.get('resource', {}).get('amount', {}).get('currency')
        # Validate amount, item_name and currency are not None
        if amount is None or item_name is None or currency is None:
            logging.error(f"Invalid JSON data: {data}")
            return jsonify({"error": "Invalid JSON data"}), 400
        # Check if the currency is in the conversion rates
        if currency not in CONVERSION_RATES:
            logging.error(f"Unsupported currency: {currency}")
            return jsonify({"error": "Unsupported currency"}), 400
        # Convert the amount to USD
        amount = int(float(amount) * CONVERSION_RATES[currency] * 100)  # convert to cents
        # Generate a new unique donation ID
        donation_id = r.incr('donation_id')
        # Use the new donation ID to create a key and store the donation information
        r.hmset(f'donation:{donation_id}', {'amount': amount, 'option': item_name, 'datetime': datetime.now().isoformat()})
        # Increase the total amount donated to an option
        r.incrby(f'total:{item_name}', amount)
        return jsonify({"success": True, "donation_id": donation_id}), 200
    except redis.exceptions.RedisError as e:
        logging.error(f"Redis error: {str(e)}")
        return jsonify({"error": f"Redis error: {str(e)}"}), 500
    except Exception as e:
        logging.error(f"Unknown error: {str(e)}")
        return jsonify({"error": f"Unknown error: {str(e)}"}), 500

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.route('/healthz', methods=['GET'])
def health_check():
    return 'Server is running!', 200

