#!/bin/bash

# Stop any running instance of the container
docker stop db_backend_dev || true
docker rm db_backend_dev || true

# Build the Docker image
docker build -t db_backend_dev .

# Run the Docker container with the volume mounted and the UWSGI_PROFILE set to 'development'
docker run -d --name db_backend_dev -p 5001:5001 -v $(pwd):/app -e UWSGI_PROFILE=development db_backend_dev

