# config.py
CONVERSION_RATES = {
    'USD': 1,
    'EUR': 1.10,  # EUR to USD rate
    'GBP': 1.35,  # GBP to USD rate
    'JPY': 0.0091,  # JPY to USD rate
    'CNY': 0.15,  # CNY to USD rate
    'INR': 0.013  # INR to USD rate
}
