# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set the working directory in the container to /app
WORKDIR /app

# Add the current directory contents into the container at /app
ADD . /app

# Install build dependencies for uWSGI and nginx
RUN apt-get update && apt-get install -y --no-install-recommends gcc libc6-dev nginx && rm -rf /var/lib/apt/lists/*

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Remove build dependencies
RUN apt-get purge -y --auto-remove gcc libc6-dev

# Setup nginx
COPY nginx*.conf /etc/nginx/

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Set the UWSGI_PROFILE environment variable to 'production' by default
ENV UWSGI_PROFILE=production

# Copy the new entrypoint script into the Docker image
COPY entrypoint.sh /entrypoint.sh
COPY uwsgi.ini /etc/uwsgi.ini
RUN rm /etc/nginx/sites-enabled/default

# change permissions to allow running as arbitrary user
RUN chmod -Rf 777 /var/log /var/cache /var/lib/nginx /var/run /etc/nginx/conf.d

# Create a new user inside the container
RUN useradd -ms /bin/bash appuser

# Adjust permissions
RUN chown -R appuser /app

# Switch to the new user
USER appuser

# Use the new script as the entrypoint
ENTRYPOINT ["/entrypoint.sh"]
